package googleDocsBlog

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.drive.Drive
import com.google.api.services.drive.model.File
import java.io.ByteArrayOutputStream
import java.net.URI
import java.net.URL

private const val APPLICATION_NAME = "Google Docs Blog Generator"
private val propertiesReader = PropertiesReader.read()
private val FOLDER_ID = propertiesReader?.getProperty("FOLDER_ID")
private val JSON_FACTORY = JacksonFactory.getDefaultInstance()

fun driveService(httpTransport: HttpTransport, credentials: Credential): Drive =
        Drive.Builder(httpTransport, JSON_FACTORY, credentials)
        .setApplicationName(APPLICATION_NAME)
        .build()

fun getDocumentList(httpTransport: HttpTransport, credentials: Credential): MutableList<File>? {
    return driveService(httpTransport, credentials).files().list()
            .setQ("'${FOLDER_ID}' in parents")
            .setFields("*")
            .execute().files ?: null
}

data class DocumentHtml(val style: String?, val body: String?)

/**
 * This hacky little replace is to remove the google redirects inserted
 */
private fun removeGoogleRedirect(text: String): String =
    Regex("https:\\/\\/www\\.google\\.com\\/url[^\"]*").replace(text) {
        Regex("(?<=q=)(?:(?!&amp).)*")
                .find(URI(it.groupValues.get(0)).query)?.groupValues?.get(0) ?: ""
    }

fun getDocument(documentId: String, httpTransport: HttpTransport, credentials: Credential): DocumentHtml {
    val output = ByteArrayOutputStream()

    driveService(httpTransport, credentials).files()
            .export(documentId, "text/html")
            .executeAndDownloadTo(output)

    val documentHtml = output.toString()

    fun getHtmlTagContent (tag: String) =
        Regex("(<${tag} .*>).*(</${tag}>)")
                .find(documentHtml)?.groupValues?.get(0)
                ?.replace(Regex("</?${tag}[^>]*>"), "")

    val style = getHtmlTagContent("style")
    var body = getHtmlTagContent("body")
    if (body != null) body = removeGoogleRedirect(body)

    return DocumentHtml(style, body)
}
